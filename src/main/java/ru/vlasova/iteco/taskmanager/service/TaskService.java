package ru.vlasova.iteco.taskmanager.service;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.repository.TaskRepository;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.util.ArrayList;
import java.util.List;

public class TaskService extends AbstractService {

    private TaskRepository taskRepository;

    private ProjectService projectService;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public Task insert(String userId, String name, String description, String dateStart, String dateFinish) {
        boolean checkGeneral = isValid(name, description, dateStart, dateFinish);
        if (!checkGeneral) return null;
        Task task = new Task(userId);
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(DateUtil.parseDateFromString(dateStart));
        task.setDateFinish(DateUtil.parseDateFromString(dateFinish));
        return task;
    }

    public void persist(Task task) throws DuplicateException {
        taskRepository.persist(task);
    }

    public List<Task> getTasks(String userId, int projectIndex) {
        if (userId == null || projectIndex < 0) return null;
        List<Project> projectList = projectService.findAll(userId);
        String projectId = projectList.get(projectIndex).getId();
        return getTasksByProjectId(userId, projectId);
    }

    public List<Task> findAll(String userId) {
        if (userId == null) return null;
        return taskRepository.findAll(userId);
    }

    public void remove(String userId, Task task) {
        if (task == null || userId == null) return;
        taskRepository.remove(task.getId());
    }

    public void remove(String userId, int id) {
        if (userId == null || id < 0) return;
        Task task = getTask(userId, id);
        if (task == null) return;
        remove(userId, task);
    }

    public Task getTask(String userId, int index) {
        if (userId == null || index < 0) return null;
        return getTaskByIndex(userId, index);
    }

    public void removeTasksByProject(String userId, Project project) {
        if (userId == null || project == null) return;
        List<Task> taskList = getTasksByProjectId(userId, project.getId());
        for (Task task : taskList) {
            taskRepository.remove(task.getId());
        }
    }

    public void removeAll(String userId) {
        if (userId == null) return;
        taskRepository.removeAll(userId);
    }

    public Task getTaskByIndex(String userId, int index) {
        if (userId == null || index < 0) return null;
        return taskRepository.findAll(userId).get(index);
    }

    private List<Task> getTasksByProjectId(String userId, String projectId) {
        if (userId == null || projectId == null || projectId.trim().isEmpty()) return null;
        List<Task> taskList = new ArrayList<>();
        List<Task> tasks = findAll(userId);
        for (Task task : tasks) {
            if (task.getProjectId().equals(projectId)) {
                taskList.add(task);
            }
        }
        return taskList;
    }

}
