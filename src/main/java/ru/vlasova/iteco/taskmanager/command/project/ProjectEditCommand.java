package ru.vlasova.iteco.taskmanager.command.project;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.ProjectService;

import java.io.IOException;

public class ProjectEditCommand extends AbctractProjectCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "project_edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected project";
    }

    @Override
    public void execute() throws IOException {
        String userId = bootstrap.getCurrentUser().getId();
        ProjectService projectService = bootstrap.getProjectService();
        System.out.println("Choose the task and type the number");
        printProjectList(userId);
        int index = Integer.parseInt(reader.readLine())-1;
        Project project = projectService.getProject(userId, index);
        if (project != null) {
            System.out.println("Editing task: " + project.getName() + ". Set new name: ");
            project.setName(reader.readLine());
            System.out.println("Task edit.");
        }
    }

}