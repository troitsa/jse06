package ru.vlasova.iteco.taskmanager.service;

import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.repository.UserRepository;
import ru.vlasova.iteco.taskmanager.util.HashUtil;

import java.util.List;

public class UserService extends AbstractService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findOne(String id) {
        return userRepository.findOne(id);
    }

    public User create(User user) throws DuplicateException {
        if (user == null || user.getRole() == null) return null;
        userRepository.persist(user);
        return user;
    }

    public void edit(User user, String login, String password) {
        if (!isValid(login, password)) return;

        user.setLogin(login);
        user.setPwd(password);
    }

    public void remove(String id) {
        userRepository.remove(id);
    }

    public void removeAll() {
        userRepository.removeAll();
    }

    public User insert(String login, String password) {
        boolean checkGeneral = isValid(login, password);
        if (!checkGeneral) return null;
        return new User(login, password);
    }

    public User login(String login, String password) {
        String id = checkUser(login);
        if(id == null) return null;
        User user = userRepository.findOne(id);
        String pwd = user.getPwd();
        if(pwd.equals(HashUtil.MD5(password))) {
            return user;
        }
        else {
            return null;
        }
    }

    private String checkUser(String login) {
        if(!isValid(login)) return null;
        List<User> users = userRepository.findAll();
        for (User user : users) {
            if(login.equals(user.getLogin())) return user.getId();
        }
        return null;
    }

}
