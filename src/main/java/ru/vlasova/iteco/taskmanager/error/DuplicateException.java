package ru.vlasova.iteco.taskmanager.error;

public class DuplicateException extends Exception {

    public DuplicateException(String message) {
        super(message);
    }

}
