package ru.vlasova.iteco.taskmanager.entity;

import java.util.Date;
import java.util.UUID;

public class Task {

    private String id = UUID.randomUUID().toString();

    private String userId="";

    private String name = "";

    private String description = "";

    private Date dateStart;

    private Date dateFinish;

    private String projectId = "";

    public Task() {
    }

    public Task(String userId) {
        this.userId = userId;
    }

    public Task(String name, String description, Date dateStart, Date dateFinish) {
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
    }

    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "    Task " + name;
    }

}
