package ru.vlasova.iteco.taskmanager.repository;

import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository implements IRepository<Task> {

    private Map<String, Task> tasks = new HashMap<>();

    @Override
    public Task persist(Task task) throws DuplicateException {
        if (tasks.containsKey(task.getId())) throw new DuplicateException("Such task exists.");
        tasks.put(task.getId(), task);
        return task;
    }

    @Override
    public void merge(Task task) {
        tasks.put(task.getId(), task);
    }

    @Override
    public List<Task> findAll(String userId) {
        List<Task> taskList = new ArrayList<>();
        for(Task task : tasks.values()) {
            if(task.getUserId().equals(userId)) {
                taskList.add(task);
            }
        }
        return taskList;
    }

    @Override
    public Task findOne(String userId, String id) {
        Task task = tasks.get(id);
        if(userId.equals(task.getUserId())) return task;
        return null;
    }

    @Override
    public void remove(String userId, String id) {
        Task task = tasks.get(id);
        if(userId.equals(task.getUserId())) {
            tasks.remove(id);
        }
    }

    @Override
    public void removeAll(String userId) {
        List<Task> taskList = findAll(userId);
        for (Task task : taskList) {
            remove(userId, task.getId());
        }
    }

    @Override
    public List<Task> findAll() {
        return new ArrayList<>(tasks.values());
    }

    @Override
    public void remove(String id) {
        tasks.remove(id);
    }

    @Override
    public void removeAll() {
        tasks.clear();
    }

    @Override
    public Task findOne(String id) {
        return tasks.get(id);
    }

}
