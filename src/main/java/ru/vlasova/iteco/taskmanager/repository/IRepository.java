package ru.vlasova.iteco.taskmanager.repository;

import java.util.List;

public interface IRepository<T> {

    List<T> findAll();

    List<T> findAll(String userId);

    T findOne(String id);

    T findOne(String userId, String id);

    T persist(T obj) throws Exception;

    void merge(T obj);

    void remove(String id);

    void remove(String userId, String id);

    void removeAll();

    void removeAll(String userId);
}
