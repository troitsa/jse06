package ru.vlasova.iteco.taskmanager.command.project;

import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.ProjectService;

public class ProjectClearCommand extends AbctractProjectCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "project_clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    public void execute() {
        ProjectService projectService = bootstrap.getProjectService();
        String userId = bootstrap.getCurrentUser().getId();
        if (userId != null || userId.trim().length() != 0) {
            projectService.removeAll(userId);
            System.out.println("All projects deleted.");
        }
    }

}