package ru.vlasova.iteco.taskmanager.service;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.repository.ProjectRepository;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.util.List;

public class ProjectService extends AbstractService {

    private ProjectRepository projectRepository;

    private TaskService taskService;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    private Project findOne(String userId, String id) {
        return projectRepository.findOne(userId, id);
    }

    public void merge(Project project) {
        projectRepository.merge(project);
    }

    public Project insert(String userId, String name, String description, String dateStart, String dateFinish) {
        boolean checkGeneral = isValid(name, description, dateStart, dateFinish);
        if (!checkGeneral) return null;
        Project project = new Project(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(DateUtil.parseDateFromString(dateStart));
        project.setDateFinish(DateUtil.parseDateFromString(dateFinish));
        return project;
    }

    public void persist(Project project) throws DuplicateException {
        projectRepository.persist(project);
    }

    public List<Project> findAll(String userId) {
        if (userId == null) return null;
        return projectRepository.findAll(userId);
    }

    public void remove(String userId, int index) {
        if (userId == null) return;
        Project project = getProjectByIndex(userId, index);
        if (project == null) return;
        projectRepository.remove(userId, project.getId());
        taskService.removeTasksByProject(userId, project);
    }

    public void remove(String userId, String id) {
        Project project = findOne(userId, id);
        if(project == null) return;
        projectRepository.remove(userId, project.getId());
        taskService.removeTasksByProject(userId, project);
    }

    public Project getProject(String userId, int index) {
        return getProjectByIndex(userId, index);
    }

    private Project getProjectByIndex(String userId, int index) {
        return projectRepository.findAll(userId).get(index);
    }

    public void removeAll(String userId) {
        projectRepository.removeAll(userId);
    }

}
