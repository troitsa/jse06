package ru.vlasova.iteco.taskmanager.command.task;

import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TaskService;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "task_clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    public void execute() {
        String userId = bootstrap.getCurrentUser().getId();
        TaskService taskService = bootstrap.getTaskService();
        if (userId != null) {
            taskService.removeAll(userId);
            System.out.println("All tasks deleted.");
        }
    }

}