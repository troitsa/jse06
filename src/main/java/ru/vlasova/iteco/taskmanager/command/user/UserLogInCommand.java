package ru.vlasova.iteco.taskmanager.command.user;

import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.UserService;

import java.io.IOException;

public class UserLogInCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public Role[] getRole() {
        return new Role[] {Role.USER};
    }

    @Override
    public String getName() {
        return "user_login";
    }

    @Override
    public String getDescription() {
        return "User authorization";
    }

    @Override
    public void execute() throws IOException {
        UserService userService = bootstrap.getUserService();
        System.out.println("Login: ");
        String login = reader.readLine();
        System.out.println("Password: ");
        String password = reader.readLine();
        User user = userService.login(login, password);
        if(user != null) {
            bootstrap.setCurrentUser(user);
            System.out.println("Log in success");
        }
        else {
            System.out.println("Login or password invalid");
        }
    }

}