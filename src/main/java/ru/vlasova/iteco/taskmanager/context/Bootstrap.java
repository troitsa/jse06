package ru.vlasova.iteco.taskmanager.context;

import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.CommandCorruptException;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.repository.ProjectRepository;
import ru.vlasova.iteco.taskmanager.repository.TaskRepository;
import ru.vlasova.iteco.taskmanager.repository.UserRepository;
import ru.vlasova.iteco.taskmanager.service.ProjectService;
import ru.vlasova.iteco.taskmanager.service.TaskService;
import ru.vlasova.iteco.taskmanager.service.UserService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskRepository taskRepository = new TaskRepository();

    private final TaskService taskService = new TaskService(taskRepository);

    private final UserRepository userRepository = new UserRepository();

    private final UserService userService = new UserService(userRepository);

    private User currentUser;

    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public UserService getUserService() {
        return userService;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    private void registry(final AbstractCommand command) {
        final String cliCommand = command.getName();
        final String cliDescription = command.getDescription();

        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();

        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();

        command.setBootstrap(this);
        commands.put(cliCommand, command);
        taskService.setProjectService(projectService);
        projectService.setTaskService(taskService);
    }

    public void start() throws Exception {
        createUsers();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (true) {
            command = reader.readLine();
            execute(command);
        }
    }

    private void execute(final String command) {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        boolean checkAccess = !abstractCommand.secure() || abstractCommand.secure() && currentUser != null;
        try {
            if (checkAccess) {
                abstractCommand.execute();
            } else {
                System.out.println("Access denied. Please, login");
            }
        } catch (DuplicateException | IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public Bootstrap init(final Class[] classes) {
        for (Class clazz : classes) {
            try {
                registry((AbstractCommand) clazz.newInstance());
            } catch (InstantiationException | IllegalAccessException e) {
                System.out.println(e.getMessage());
            }
        }
        return this;
    }

    private void createUsers() {
        final User user = new User(Role.USER);
        user.setLogin("user");
        user.setPwd("qwerty");
        final User admin = new User(Role.ADMIN);
        admin.setLogin("admin");
        admin.setPwd("1234");
        try {
            userService.create(user);
            userService.create(admin);
        } catch (DuplicateException e) {
            System.out.println(e.getMessage());
        }
        setCurrentUser(user);
    }

}