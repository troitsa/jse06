package ru.vlasova.iteco.taskmanager.command.system;

import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

public class HelpCommand extends AbstractCommand {
    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for(final AbstractCommand command : bootstrap.getCommands()) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }

}