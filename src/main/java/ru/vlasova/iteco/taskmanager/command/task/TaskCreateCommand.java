package ru.vlasova.iteco.taskmanager.command.task;

import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.service.TaskService;

import java.io.IOException;

public class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "task_create";
    }

    @Override
    public String getDescription() {
        return "Create new task";
    }

    @Override
    public void execute() throws IOException, DuplicateException {
        String userId = bootstrap.getCurrentUser().getId();
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("Creating task. Set name: ");
        String name = reader.readLine();
        System.out.println("Input description: ");
        String description = reader.readLine();
        System.out.println("Set start date: ");
        String dateStart = reader.readLine();
        System.out.println("Set end date: ");
        String dateFinish = reader.readLine();
        Task task = taskService.insert(userId, name, description, dateStart, dateFinish);
        taskService.persist(task);
        System.out.println("Task created.");
    }

}