package ru.vlasova.iteco.taskmanager.command.task;

import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TaskService;

import java.io.IOException;
import java.util.List;

public class TaskRemoveCommand extends AbstractTaskCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "task_remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task";
    }

    @Override
    public void execute() throws IOException {
        String userId = bootstrap.getCurrentUser().getId();
        if(userId == null) return;
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("Choose the task and type the number");
        List<Task> taskList = taskService.findAll(userId);
        if (taskList.size() !=0) {
            printTaskList(taskList);
            int index = Integer.parseInt(reader.readLine()) - 1;
            Task task = taskService.getTask(userId, index);
            if (task != null) {
                taskService.remove(userId, task);
                System.out.println("Task deleted.");
            }
        }
    }

}