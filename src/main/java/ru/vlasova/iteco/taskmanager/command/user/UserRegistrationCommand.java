package ru.vlasova.iteco.taskmanager.command.user;

import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.service.UserService;

import java.io.IOException;

public class UserRegistrationCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public Role[] getRole() {
        return new Role[] {Role.USER};
    }

    @Override
    public String getName() {
        return "user_registation";
    }

    @Override
    public String getDescription() {
        return "To register new user";
    }

    @Override
    public void execute() throws IOException, DuplicateException {
        UserService userService = bootstrap.getUserService();
        System.out.println("Creating user. Set login: ");
        String login = reader.readLine();
        System.out.println("Set password: ");
        String password = reader.readLine();
        User user = userService.insert(login, password);
        if(user == null) {
            System.out.println("User is not registered. Try again");
        } else {
            userService.create(user);
            System.out.println("User is registered.");
        }
    }

}