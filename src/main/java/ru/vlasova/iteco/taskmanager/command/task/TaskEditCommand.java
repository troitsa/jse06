package ru.vlasova.iteco.taskmanager.command.task;

import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TaskService;

import java.io.IOException;
import java.util.List;

public class TaskEditCommand  extends AbstractTaskCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "task_edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected task";
    }

    @Override
    public void execute() throws IOException {
        String userId = bootstrap.getCurrentUser().getId();
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("Choose the task and type the number");
        List<Task> taskList = taskService.findAll(userId);
        if (taskList.size() !=0) {
            printTaskList(taskList);
            int index = Integer.parseInt(reader.readLine()) - 1;
            Task task = taskService.getTask(userId, index);
            if (task != null) {
                System.out.println("Editing task: " + task.getName() + ". Set new name: ");
                task.setName(reader.readLine());
                System.out.println("Set new description: ");
                task.setDescription(reader.readLine());
                System.out.println("Choose project id: ");
                printProjectList(userId);
                int projectIndex = Integer.parseInt(reader.readLine())-1;
                task.setProjectId(bootstrap.getProjectService().getProject(userId, projectIndex).getId());
                System.out.println("Task edit.");
            }
        }
    }

}