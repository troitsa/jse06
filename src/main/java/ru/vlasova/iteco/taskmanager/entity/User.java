package ru.vlasova.iteco.taskmanager.entity;

import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.util.HashUtil;

import java.util.UUID;

public class User {

    private String id = UUID.randomUUID().toString();

    private String login = "";

    private String pwd = "";

    private Role role = Role.USER;

    public User() {
    }

    public User(Role role) {
        this.role = role;
    }

    public User(String login, String pwd) {
        this.login = login;
        this.pwd = HashUtil.MD5(pwd);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = HashUtil.MD5(pwd);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "User: " + login + ", " + role;
    }
}
