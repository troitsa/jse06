package ru.vlasova.iteco.taskmanager.command.user;

import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.UserService;

import java.io.IOException;

public class UserEditCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public Role[] getRole() {
        return new Role[] {Role.USER, Role.ADMIN};
    }

    @Override
    public String getName() {
        return "user_edit";
    }

    @Override
    public String getDescription() {
        return "Edit user";
    }

    @Override
    public void execute() throws IOException {
        UserService userService = bootstrap.getUserService();
        System.out.println("Edit user. Set new login: ");
        String login = reader.readLine();
        System.out.println("Set new password: ");
        String password = reader.readLine();
        User user = bootstrap.getCurrentUser();
        userService.edit(user, login, password);
    }

}