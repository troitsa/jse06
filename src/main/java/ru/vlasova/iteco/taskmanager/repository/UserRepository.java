package ru.vlasova.iteco.taskmanager.repository;

import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRepository implements IRepository<User> {

    private Map<String, User> users = new HashMap<>();

    @Override
    public User persist(User user) throws DuplicateException {
        if (users.containsKey(user.getId())) throw new DuplicateException("Such user exists.");
        users.put(user.getId(), user);
        return user;
    }

    @Override
    public void merge(User user) {
        users.put(user.getId(), user);
    }

    @Override
    public List<User> findAll(String userId) {
        return findAll();
    }

    @Override
    public User findOne(String userId, String id) {
        return findOne(id);
    }

    @Override
    public void remove(String userId, String id) {
        remove(id);
    }

    @Override
    public void removeAll(String userId) {
        removeAll();
    }

    @Override
    public List<User> findAll() {
        return new ArrayList<>(users.values());
    }

    @Override
    public User findOne(String id) {
        return users.get(id);
    }

    @Override
    public void remove(String id) {
        users.remove(id);
    }

    @Override
    public void removeAll() {
        users.clear();
    }

}
