package ru.vlasova.iteco.taskmanager.command.project;

import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.ProjectService;

import java.io.IOException;

public class ProjectRemoveCommand extends AbctractProjectCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "project_remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project";
    }

    @Override
    public void execute() throws IOException {
        String userId = bootstrap.getCurrentUser().getId();
        ProjectService projectService = bootstrap.getProjectService();
        printProjectList(userId);
        System.out.println("To delete project choose the project and type the number");
        int index = Integer.parseInt(reader.readLine())-1;
        projectService.remove(userId, index);
        System.out.println("Project delete.");
    }

}