package ru.vlasova.iteco.taskmanager.command.system;

import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import java.io.IOException;

public class ExitCommand extends AbstractCommand {
    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit from application.";
    }

    @Override
    public void execute() throws IOException {
        reader.close();
        System.exit(1);
    }

}