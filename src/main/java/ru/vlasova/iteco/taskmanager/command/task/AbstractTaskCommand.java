package ru.vlasova.iteco.taskmanager.command.task;

import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.ProjectService;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public Role[] getRole() {
        return new Role[] {Role.USER};
    }

    public void printTaskList(List<Task> taskList) {
        int i = 1;
        for(Task task : taskList) {
            System.out.println(i + ": " + task);
            i++;
        }
    }

    public void printProjectList(String userId) {
        ProjectService projectService = bootstrap.getProjectService();
        List<Project> projectList = projectService.findAll(userId);
        if(projectList != null && projectList.size()!=0) {
            int i = 1;
            for (Project project : projectList) {
                System.out.println(i++ + ": " + project);
            }
        }
    }

}
