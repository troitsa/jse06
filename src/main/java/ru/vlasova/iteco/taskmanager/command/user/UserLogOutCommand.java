package ru.vlasova.iteco.taskmanager.command.user;

import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import java.io.IOException;

public class UserLogOutCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public Role[] getRole() {
        return new Role[] {Role.USER};
    }

    @Override
    public String getName() {
        return "user_logout";
    }

    @Override
    public String getDescription() {
        return "Logout user";
    }

    @Override
    public void execute() {
        bootstrap.setCurrentUser(null);
        System.out.println("Log out success");
    }

}