package ru.vlasova.iteco.taskmanager.repository;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.*;

public class ProjectRepository implements IRepository<Project> {

    private Map<String, Project> projects = new HashMap();

    @Override
    public Project findOne(String id) {
        return projects.get(id);
    }

    @Override
    public Project persist(Project project) throws DuplicateException {
        if (projects.containsKey(project.getId())) throw new DuplicateException("Such project exists.");
        projects.put(project.getId(), project);
        return project;
    }

    @Override
    public void merge(Project project) {
        projects.put(project.getId(), project);
    }

    @Override
    public List<Project> findAll(String userId) {
        List<Project> projectList = new ArrayList<>();
        for(Project project : projects.values()) {
            if(project.getUserId().equals(userId)) {
                projectList.add(project);
            }
        }
        return projectList;
    }

    @Override
    public Project findOne(String userId, String id) {
        Project project = projects.get(id);
        if(userId.equals(project.getUserId())) return project;
        return null;
    }

    @Override
    public void remove(String userId, String id) {
        Project project = projects.get(id);
        if(userId.equals(project.getUserId())) {
            projects.remove(id);
        }
    }

    @Override
    public void removeAll(String userId) {
        List<Project> projects = findAll(userId);
        for (Project project : projects) {
            remove(userId, project.getId());
        }
    }

    @Override
    public void removeAll() {
        projects.clear();
    }

    @Override
    public List<Project> findAll() {
        return new ArrayList<>(projects.values());
    }

    @Override
    public void remove(String id) {
        projects.remove(id);
    }

}
