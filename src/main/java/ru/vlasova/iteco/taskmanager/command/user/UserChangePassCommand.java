package ru.vlasova.iteco.taskmanager.command.user;

import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.UserService;

import java.io.IOException;

public class UserChangePassCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public Role[] getRole() {
        return new Role[] {Role.USER, Role.ADMIN};
    }

    @Override
    public String getName() {
        return "user_change_pass";
    }

    @Override
    public String getDescription() {
        return "Change user password";
    }

    @Override
    public void execute() throws IOException {
        UserService userService = bootstrap.getUserService();
        User user = bootstrap.getCurrentUser();
        System.out.println("Set new password: ");
        String password = reader.readLine();
        if(password == null) {
            System.out.println("Invalid password");
        }
        else {
            userService.edit(user, user.getLogin(), password);
            System.out.println("Password changed");
        }
    }

}
