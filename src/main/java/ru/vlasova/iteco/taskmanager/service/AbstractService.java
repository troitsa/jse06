package ru.vlasova.iteco.taskmanager.service;

public abstract class AbstractService {

    protected boolean isValid(String... strings){
        for (String str : strings)
            if (str == null || str.isEmpty())
                return false;
        return true;
    }

}