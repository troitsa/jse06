package ru.vlasova.iteco.taskmanager.enumeration;

public enum Role {

    USER("User"),
    ADMIN("Administrator");

    private String roleName;

    Role(String roleName) {
        this.roleName = roleName;
    }

    public String displayName() {
        return roleName;
    }

}
