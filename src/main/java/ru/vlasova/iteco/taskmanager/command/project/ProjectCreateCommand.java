package ru.vlasova.iteco.taskmanager.command.project;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.service.ProjectService;

import java.io.IOException;

public class ProjectCreateCommand extends AbctractProjectCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "project_create";
    }

    @Override
    public String getDescription() {
        return "Create new project";
    }

    @Override
    public void execute() throws IOException, DuplicateException {
        ProjectService projectService = bootstrap.getProjectService();
        String userId = bootstrap.getCurrentUser().getId();
        if (userId == null) return;
        System.out.println("Creating project. Set name: ");
        String name = reader.readLine();
        System.out.println("Input description: ");
        String description = reader.readLine();
        System.out.println("Set start date: ");
        String dateStart = reader.readLine();
        System.out.println("Set end date: ");
        String dateFinish = reader.readLine();
        Project project = projectService.insert(userId, name, description, dateStart, dateFinish);
        projectService.persist(project);
        System.out.println("Project created.");
    }

}